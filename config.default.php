<?php
$config = [
	// Warning shown below the new wiki form (allows HTML formatting)
	'newWikiWarning' => '',
	'phabricatorUrl' => 'https://phabricator.wikimedia.org',
	'gerritUrl' => 'https://gerrit.wikimedia.org',
	// Link to a status page, e.g. on https://grafana.wmcloud.org/
	'statusUrl' => '',
	// Require that patches are V+2 before building the wiki
	'requireVerified' => true,
	// Additional paths, e.g. for npm when using nvm
	'extraPaths' => [],
	// OAuth config. When enabled only authenticated users can create
	// wikis, and can delete their own wikis.
	'oauth' => [
		'url' => null,
		'callback' => null,
		'key' => null,
		'secret' => null,
		// OAuth admins can delete any wiki
		'admins' => []
	],
	// Conduit API key for bot cross-posting to Phabricator
	'conduitApiKey' => null,
	// Read only mode disables wiki creation
	'readOnly' => false,
	'readOnlyText' => 'This patchdemo instance is in read only mode. You may visit or delete previously created wikis,' .
		' but no new wikis can be created.',
];
